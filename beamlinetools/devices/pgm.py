import numpy as np

from ophyd import Component as Cpt
from ophyd import EpicsSignal, EpicsSignalRO
from bessyii_devices.positioners import PVPositioner


from bessyii_devices.pgm import PGM, MonoAlphaBetaAxis, MonoThetaAxis
from bessyii_devices.pgm import FlyingEnergy


class FlyingWithPrecision(FlyingEnergy):
    def describe(self):
        ret = super().describe()
        ret[self.name]['precision'] = 4
        return ret

class PGMSlit(PVPositioner):
    readback = Cpt(EpicsSignalRO,  'slitwidth',      kind='normal')
    setpoint = Cpt(EpicsSignal,    'SetSlitWidth',     kind='normal')
    done = Cpt(EpicsSignalRO, 'ES_0_STATUS', kind='omitted')
    done_value = 0

    def describe(self):
        ret = super().describe()
        ret[self.readback.name]['precision'] = 4
        return ret
    
class PGMOptics(PGM):
    
    alpha            = Cpt(MonoAlphaBetaAxis, '',  ch_name='Alpha', settle_time=10.0, kind='config', labels={'pgm'})
    beta             = Cpt(MonoAlphaBetaAxis, '',  ch_name='Beta',  settle_time=10.0, kind='config', labels={'pgm'})
    theta            = Cpt(MonoThetaAxis, '',  ch_name='Theta', settle_time=10.0, kind='config', labels={'pgm'})
    en               = Cpt(FlyingWithPrecision,'')
    slit             = Cpt(PGMSlit, '',  )
    read_attrs       = ['en.readback', 'slit.readback']
    
