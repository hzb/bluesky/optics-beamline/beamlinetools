# Notes on Keithleys

These are notes which we should later formalise to give users the info they need to use the keithleys in the following two scenarios:

1. high accuracy stepwise scanning over large current ranges (multiple orders of magnitude)
2. high speed acquisitions in continuous mode over small ranges

The operation of the Keithley (6514 and 6517B) cannot be the same in these two operation modes because they have different requirements. 

In these notes I will try to describe how the Keithely is configured to operate in each case.

## Critical Parameters

The keithleys have the following critical parameters and settings

1. **Autorange** or **Fixed Range**
2. **Integration Time** (inverse of NPLC). This parameter defines the amount of time the device integrates for before it returns a result. You should read the Keithley manual to understand optimal setting of this parameter. Using integration time above 20 ms (1 NPLC) achieves diminishing returns in terms of signal to noise ratio improvements. Less than 20 ms and you introduce additional measurement noise. This might be acceptable if you care a lot about acquiring quickly.
3. **Number of points averaged over**. Icreasing this above 1 means that the keithley will acquire N measurements, each for integration time before returning the average of these points. 

On the Keithley Phoebus panel there is an additional parameter displayed labelled "POLL" This is the rate at which the IOC triggers and reads the Keithley. During an acquisition you will see this be changed to Passive because the control of triggering is held by the client application Bluesky. 

For spec, this is left at 0.1 because spec generally just get's the last available value from the IOC. 

## Error handling and Autoranging

When a Keithley crosses over a boundry between one current range and another it searches each of it's available range settings until it finds the most appropriate setting. This can take more than 2 seconds. 

If you send a trigger command and request a new value during this time the first value you get back will not be valid. You must take the second value. This is an annoying feature of the keithley. There is no way to query the device to know it's in an auto-range state. 

To deal with this the Keithley IOC [implements some simple error handling](https://codebase.helmholtz.cloud/hzb/epics/support/keithley/-/blob/d4a0a8bfceb836e15e8e3af4cd5837cbaef9d67a/keithleyApp/Db/Keithley6514.proto#L86) The following pseudocode is implemented.

```
send trigger

wait for response

if no response in 2 seconds:

    send trigger again

    wait for read value

    read value

else:

    read value
```

This allows us to wait for the keithley to recover from an error state like an auto range and send us the first value it recieves. I do not implement the second value reading here because in the case we operate in fixed range mode, I always want the first value. 

## High Accuracy Over Large Range

In this mode of operation the user wants to measure as accurately as possible. Possibly over a large range which means they need to use autoranging. 

This means we have to deal with the bad value that we get back after an autorange. We need to trigger the device twice, once to get the first value after the autorange, and then again to get the first "good" value. 

Unfortunately, since there is no way of querying the device to know we are in an autorange state, we have to send these two triggers every time we want to read. If the integration time is only 20ms this isn't that big a problem. (There might be a way we can get the IOC to report it has had a timeout which would tell us we need to send another trigger...)

The Integration time should be set to be 20ms. You can set it higher, but you will not improve the SNR much. You should not set it less since you care about accuracy in this scenario.


## High Speed Over a Limited Range

As a result of the autoranging problem it is **not** possible to perform fast scans over multiple keithley ranges. 

If you're focus is on speed, as is the case for continuous measurements, then you can *only* operate in fixed range mode because you cannot afford to have the keithley hang for 2 seconds while you're monochromator is still sweeping. 

Since you're focussed on speed you could also set the integration time to be smaller than 20ms. 

The keithley ophyd device puts the IOC into a passive mode, and (depending on the model) configures the IOC to acquire as quickly as it possibly can. 

```
def stage(self):

        self.scan.put('Passive')        # update the EPICS PV as quick as we can, 
        self.front_panel.put('On')      # Turn the front panel on (might be bad for readback)
        self.avg_type.put('Moving')     # Moving average filter, for speed of readback
        self.arm_src.put('Immediate')   # Immediate arm to give the fastest update possible
        self.trig_src.put('Immediate')  # Immediate trigger to give the fastest update possible



```