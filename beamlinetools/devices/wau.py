
from ophyd import Component as Cpt
from ophyd import EpicsSignal
from bessyii_devices.axes import AxisTypeD
from ophyd import Device

# the stop_signal works manuallz but not with ctrl+c
class AxysTypeDOptics(AxisTypeD):
    stop_signal     = Cpt(EpicsSignal,  '.STOP', kind='omitted')

    def __init__(self, prefix, **kwargs):
        super().__init__(prefix, **kwargs)
        self.readback.name = self.name

# WA1
class WAU(Device):
    #_default_read_attrs = ['top.readback', 'bottom.readback', 'left.readback', 'right.readback']
    top         = Cpt(AxysTypeDOptics, 'wau1top', labels={"apertures"})
    bottom      = Cpt(AxysTypeDOptics, 'wau1bottom', labels={"apertures"})
    left        = Cpt(AxysTypeDOptics, 'wau1left', labels={"apertures"}) # wall in old convention
    right       = Cpt(AxysTypeDOptics, 'wau1right', labels={"apertures"}) #ring in old convention



