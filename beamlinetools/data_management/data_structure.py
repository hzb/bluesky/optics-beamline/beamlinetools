import time
import os
import socket
from datetime import datetime

from event_model import RunRouter
from beamlinetools.callbacks.file_exporter import CSVCallback
from beamlinetools.data_management.specfile_management import spec_factory, new_spec_file


class OpticsDataStructure:
    """
    This class is used to create the data structure needed at Optics Beamline.

    The class should be initialized in a startup file like this:
        from beamlinetools.data_management.data_structure import OpticsDataStructure
        mds = OpticsDataStructure()
    
    Then from the IPython session one can create a new experiment:
        mds.change_user('<experimental_group>')
    
    For different parts of the experiment, a new folder with a new specfile
    can be created with:
        mds.change_experiment('<experiment_description>')
    """

    def __init__(self, RE):
        """
        Initialize the OpticsDataStructure class.
        
        Args:
            RE: RunEngine instance
        """
        self.RE = RE
        self._exp_name = None  # Defined by newdata or read from RE.md
        self._bluesky_data = "/opt/bluesky/data"
        self._exp_name = None
        self.csv = None
        self._base_name = None
        
        self._commissioning_folder = str(datetime.now().year) + "_commissioning"
        self._commissioning_abs_path = os.path.join(self._bluesky_data, self._commissioning_folder)
        
        self._create_commissioning_folder()
        self._load_existing_keys()
        self._init_and_subscribe_to_csv_export()
        self._init_and_subscribe_to_csv_export_commissioning()
        self._set_csv_export_folder()
        self._set_csv_export_folder_commissioning()
        self._init_and_subscribe_spec_factory()
        self._init_and_subscribe_spec_factory_commissioning()
        self.print_info()
    
    def print_info(self):
        """Print information about the data export location."""
        if self._exp_name is not None and self._base_name is not None:
            data_folder = os.path.join("/home/opticsmaster/bluesky/", self._exp_name, self._base_name)
            msg = f"The data will be exported in \n{data_folder}"
        else:
            data_folder = os.path.join("/home/opticsmaster/bluesky/", self._commissioning_folder)
            msg = f"The data will be exported in \n{data_folder}"
        print()
        print()
        print("##################################################################")
        print(msg)
        print("##################################################################")
        print()
        print()

    def _init_and_subscribe_to_csv_export(self):
        """Initialize and subscribe to CSV export."""
        self.csv = CSVCallback(file_path=self._bluesky_data)
        self.RE.subscribe(self.csv.receiver)

    def _init_and_subscribe_to_csv_export_commissioning(self):
        """Initialize and subscribe to CSV export for commissioning."""
        self.csv_commissioning = CSVCallback(file_path=self._bluesky_data)
        self.RE.subscribe(self.csv_commissioning.receiver)

    def _set_csv_export_folder(self):
        """Set the folder for CSV export."""
        self.csv.change_user(os.path.join(self._bluesky_data, self._exp_name, self._base_name))

    def _set_csv_export_folder_commissioning(self):
        """Set the folder for CSV export during commissioning."""
        self.csv_commissioning.change_user(self._commissioning_folder)

    def _init_and_subscribe_spec_factory(self):
        """Initialize and subscribe to spec factory."""
        spec_factory.file_prefix = self._base_name
        spec_factory.directory = os.path.join(self._bluesky_data, self._exp_name, self._base_name)

        rr = RunRouter([spec_factory])
        self.RE.subscribe(rr)
    
    def _init_and_subscribe_spec_factory_commissioning(self):
        """Initialize and subscribe to spec factory for commissioning."""
        year = time.strftime('%Y')
        spec_factory.file_prefix = 'optics' + year
        spec_factory.directory = self._commissioning_abs_path
        spec_factory.directory = os.path.join(self._bluesky_data, self._exp_name, self._base_name)

        rr_com = RunRouter([spec_factory])
        self.RE.subscribe(rr_com)
    
    def commissioning(self):
        """Set up CSV export folder and specfile for commissioning."""
        self.csv.change_user(self._commissioning_folder)
        
        year = time.strftime('%Y')
        spec_factory.file_prefix = 'optics' + year
        spec_factory.directory = self._commissioning_abs_path
        
    def _create_commissioning_folder(self):
        """Create the commissioning folder if it doesn't exist."""
        if not os.path.exists(self._commissioning_abs_path):
            os.makedirs(self._commissioning_abs_path)
        
    def _load_existing_keys(self):
        """Load existing keys from RunEngine metadata."""
        try:
            self._exp_name = self.RE.md["exp_name"]
            self._experiment_dir = os.path.join(self._bluesky_data, self._exp_name)
        except KeyError:
            self._exp_name = None
        
        try:
            self._base_name = self.RE.md["base_name"]
        except KeyError:
            self._base_name = None

        try:
            self._newdata_dir = self.RE.md['exported_data_dir']
        except KeyError:
            self._newdata_dir = None

    def change_user(self, exp_name):
        """
        Change the user/experimental group.

        Args:
            exp_name (str): Name of the experimental group.
        """
        self.RE.md["exp_name"] = exp_name
        self._exp_name = exp_name
        self._experiment_dir = os.path.join(self._bluesky_data, self._exp_name)
        
        if not os.path.exists(self._experiment_dir):
            os.makedirs(self._experiment_dir)
        
    def change_experiment(self, base_name):
        """
        Change the experiment description and create a new folder.

        Args:
            base_name (str): Description of the experiment.
        """
        self._base_name = base_name   
        self._newdata_dir = os.path.join(self._experiment_dir, self._base_name)
        
        if not os.path.exists(self._newdata_dir):
            os.makedirs(self._newdata_dir)
            print(f'Created new folder {self._newdata_dir}')
        else:
            raise ValueError(f'Existing folder detected {self._newdata_dir}. This should not happen, ask beamline scientist what to do')
        
        self.csv.change_user(self._newdata_dir)  # Change folder for CSV export
        spec_factory.file_prefix = self._base_name  # Change specfile name
        spec_factory.directory = self._newdata_dir

        # Update RE.md with new values
        self.RE.md['base_name'] = self._base_name
        self.RE.md['specfile'] = os.path.join(self._newdata_dir, self._base_name)
        self.RE.md['hostname'] = socket.gethostname()
        self.RE.md['exported_data_dir'] = self._newdata_dir
        
        if 'scan_id' in self.RE.md.keys():
            print(f'Next scan is number: {self.RE.md["scan_id"] + 1}')
        else:
            print(f'Next scan is number: 0')
