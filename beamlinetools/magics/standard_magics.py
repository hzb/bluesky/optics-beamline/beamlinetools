import asyncio
import numpy as np 
from bluesky import RunEngine, RunEngineInterrupted
from IPython.core.magic import Magics, magics_class, line_magic
from datetime import datetime
import bluesky.plans as bp
import bluesky.plan_stubs as bps

try:
    # cytools is a drop-in replacement for toolz, implemented in Cython
    from cytoolz import partition
except ImportError:
    from toolz import partition


from bluesky.magics import _ct_callback, get_labeled_devices
from bluesky.magics import MetaclassForClassProperties

from beamlinetools.utils.pbar_bessy import ProgressBarManager
# from bluesky.utils import ProgressBarManager
from beamlinetools.magics.standard_magics_utils import color_generator


@magics_class
class BlueskyMagicsBessy(Magics, metaclass=MetaclassForClassProperties):
    """
    IPython magics for bluesky at BESSYII.

    Usage:

    label_axes_dict = {
                        # "dcm": ["dcm.monoz.user_readback", 
                        #        ]
                        }
    get_ipython().register_magics(BlueskyMagicsBessy(RE, get_ipython(), database_name ="db", label_axes_dict=label_axes_dict))

    """
    def __init__(self, RE, shell, label_axes_dict={}, database_name='db', **kwargs) -> None:
        """
        Initializes a BlueskyMagicsBessy instance.

        Args:
            RE: A RunEngine instance.
            shell: IPython shell instance.
            label_axes_dict: A dictionary describing devices and axes names.
            exclude_labels_from_wa: devices to be excluded from the magics
            database_name: Name of the database.
            **kwargs: Additional keyword arguments.

        Returns:
            None
        """
        super().__init__( **kwargs)
        self.RE = RE
        self.shell = shell
        self.decimals = 4
        self.pbar_manager = ProgressBarManager()
        self.color_gen = color_generator()
        self.label_axes_dict = label_axes_dict
        self.database_name = database_name
        self.plotted_lines = []

    
    def _ensure_idle(self):
        """
        Ensure that the RunEngine is in the 'idle' state.

        If the RunEngine state is not 'idle', it aborts the current run.

        Returns:
            None
        """
        if self.RE.state != 'idle':
            print('The RunEngine invoked by magics cannot be resumed.')
            print('Aborting...')
            self.RE.abort()
    
    def _get_device_from_ns(self, device_name):
        """
        Get a device object from the user namespace.

        Args:
            device_name (str): The name of the device.

        Returns:
            Any: The device object.
        
        Raises:
            ValueError: If the device is not found in the user namespace.
        """
        if "_" in device_name:
            device_name = device_name.replace("_",".")
        try:
            dev = eval(device_name, self.shell.user_ns)
            if dev is not None:
                return dev
            else:
                raise ValueError(f"Device {device_name} is {dev }")

        except KeyError:
            raise ValueError(f"Device {device_name} not found in user namespace.")

    def find_motor_detector(self):
        """
        Find the motor and detector from the most recent run.

        Returns:
            tuple: A tuple containing the motor and detector objects.
        """
        db        = eval(self.database_name, self.shell.user_ns)
        run       = db[-1]
        detector  = run.metadata['start']['detectors']
        motor     = run.metadata['start']['motors'][0]
        return motor, detector
    
    def get_motor_position(self, motor):
        """
        Get the position of a motor.

        Args:
            motor (str): The name of the motor.

        Returns:
            float: The position of the motor.

        Raises:
            NotImplementedError: If the position cannot be obtained using both primary and alternative methods.
        """
        motor = self._get_device_from_ns(motor)
        try:
            motor_pos = motor.readback.get()
        except Exception as e:
            pass
            #print(f'Exception attempting primary method: {e}')
            try:
                # If the primary method fails, try an alternative method
                motor_pos = motor.user_readback.get()
            except Exception as e:
                # Handle failure of the alternative method
                print(f'Exception attempting alternative method: {e}')
                raise NotImplementedError('Unable to find the motor position using both primary and alternative methods')
        return motor_pos

    def plot_motors_current_pos(self):
        """
        Plot the current positions of motors.

        Returns:
            tuple: A tuple containing the motor and its current position.
        """
        bec        = self.shell.user_ns.get('bec')
        live_plots = bec._live_plots
        live_plot_key = list(live_plots.keys())[0]
        live_plot_dets = live_plots[live_plot_key].keys()
        motor, detectors = self.find_motor_detector()
        x_motor = self.get_motor_position(motor)
        color = next(self.color_gen)
        if len(self.plotted_lines)>0:
            for l in self.plotted_lines:
                l.remove()
        self.plotted_lines=[]
        for det in live_plot_dets:
            live_plot = live_plots[live_plot_key][det]
            line = live_plot.ax.axvline(x=x_motor,ymin=-1e30,ymax=+1e30, 
                                 color=color, linestyle='dashed')
            self.plotted_lines.append(line)

        return motor, x_motor
    
    def find_motor_and_positon(self,position):
        """
        Find the motor and its position.

        Args:
            position (str): The position identifier ('min', 'max', 'pic', 'cen', 'com').

        Returns:
            tuple: A tuple containing the motor name and its position.

        Raises:
            None
        """
        db        = eval(self.database_name, self.shell.user_ns)
        run       = db[-1]
        detector  = run.metadata['start']['detectors'][0]
        motor     = run.metadata['start']['motors'][0]
        bec        = self.shell.user_ns.get('bec')
        peak_dict = bec.peaks
        mot_pos   = peak_dict[position][detector]
        if "_" in motor:
            motor = motor.replace("_",".")
        if position not in ['cen','com']:
            mot_pos = mot_pos[0]
        return motor, mot_pos
    
    def move_to_pos(self, motor, motor_position):
        """
        Move the motor to the specified position.

        Args:
            motor (str): The name of the motor to move.
            motor_position (float): The position to move the motor to.

        Returns:
            None

        Raises:
            None
        """
        plan = bps.mv(motor,motor_position)
        self.RE.waiting_hook = self.pbar_manager
        try:
            self.RE(plan)
        except RunEngineInterrupted:
            ...
        self.RE.waiting_hook = None
        self._ensure_idle()

    @line_magic
    def where(self, line):
        """
        Print the current position of the motor, and plot it.

        Args:
            line (str): Not used.

        Returns:
            None
        """
        motor, x_motor = self.plot_motors_current_pos()
        print(f'The motor: {motor} is at the position {x_motor}')

    @line_magic
    def cen(self, line):
        """
        Move the motor to the center position, and plot it..

        Args:
            line (str): Not used.

        Returns:
            None
        """
        motor_and_axis, mot_pos = self.find_motor_and_positon('cen')
        mot = self._get_device_from_ns(motor_and_axis)
        self.move_to_pos(mot, mot_pos)
        self.plot_motors_current_pos()
        print('Moved motor', motor_and_axis, 'to position', mot_pos)


    @line_magic
    def pic(self, line):
        """
        Move the motor to the max position, and plot it.

        Args:
            line (str): Not used.

        Returns:
            None
        """
        motor_and_axis, mot_pos = self.find_motor_and_positon('max')
        mot = self._get_device_from_ns(motor_and_axis)
        self.move_to_pos(mot, mot_pos)
        self.plot_motors_current_pos()
        print('Moved motor', motor_and_axis, 'to position', mot_pos)

    @line_magic
    def com(self, line):
        """
        Move the motor to the center of mass position, and plot it.

        Args:
            line (str): Not used.

        Returns:
            None
        """
        motor_and_axis, mot_pos = self.find_motor_and_positon('com')
        mot = self._get_device_from_ns(motor_and_axis)
        self.move_to_pos(mot, mot_pos)
        self.plot_motors_current_pos()
        print('Moved motor', motor_and_axis, 'to position', mot_pos)

    @line_magic
    def minimum(self, line):
        """
        Move the motor to the minimum position, and plot it.

        Args:
            line (str): Not used.

        Returns:
            None
        """
        motor_and_axis, mot_pos = self.find_motor_and_positon('min')
        mot = self._get_device_from_ns(motor_and_axis)
        self.move_to_pos(mot, mot_pos)
        self.plot_motors_current_pos()
        print('Moved motor', motor_and_axis, 'to position', mot_pos)

    @line_magic
    def mov(self, line):
        """
        Move the motor(s) to the specified position(s).

        Args:
            line (str): The line containing motor and position pairs.

        Returns:
            None
        """
        if len(line.split()) % 2 != 0:
            raise TypeError("Wrong parameters. Expected: "
                            "%mov motor position (or several pairs like that)")
        args = []
        for motor, pos in partition(2, line.split()):
            args.append(eval(motor, self.shell.user_ns))
            args.append(eval(pos, self.shell.user_ns))
        plan = bps.mv(*args)
        self.pbar_manager.user_ns=self.shell.user_ns
        self.RE.waiting_hook = self.pbar_manager
        try:
            self.RE(plan)
        except RunEngineInterrupted:
            ...
        self.RE.waiting_hook = None
        self._ensure_idle()
        return None

    @line_magic
    def movr(self, line):
        if len(line.split()) % 2 != 0:
            raise TypeError("Wrong parameters. Expected: "
                            "%mov motor position (or several pairs like that)")
        args = []
        for motor, pos in partition(2, line.split()):
            args.append(eval(motor, self.shell.user_ns))
            args.append(eval(pos, self.shell.user_ns))
        plan = bps.mvr(*args)
        self.pbar_manager.user_ns=self.shell.user_ns
        self.RE.waiting_hook = self.pbar_manager
        try:
            self.RE(plan)
        except RunEngineInterrupted:
            ...
        self.RE.waiting_hook = None
        self._ensure_idle()
        return None

    def calculate_relative_movement(self, motor:str, target_real_position:float):
        try:
            current_absolute_position = eval(motor+'.abs_readback.get()', self.shell.user_ns)
        except Exception as e:
                raise NotImplementedError(f'The motor {motor} can not be moved in absoulte coordinates. Use "mov" or "movr"\n',
                                          f'No "abs_readback.get() available"\n',
                                          f'Exception: {e}')
        relative_movement = float(target_real_position) - float(current_absolute_position) 
        return str(relative_movement)

    @line_magic
    def mova(self, line):
        if len(line.split()) % 2 != 0:
            raise TypeError("Wrong parameters. Expected: "
                            "%mov motor position (or several pairs like that)")
        args = []
        for motor, pos in partition(2, line.split()):
            print(motor, pos)
            relative_movement = self.calculate_relative_movement(motor, pos)
            args.append(eval(motor, self.shell.user_ns))
            args.append(eval(relative_movement, self.shell.user_ns))
        
        plan = bps.mvr(*args)
        self.pbar_manager.user_ns=self.shell.user_ns
        self.RE.waiting_hook = self.pbar_manager
        try:
            self.RE(plan)
        except RunEngineInterrupted:
            ...
        self.RE.waiting_hook = None
        self._ensure_idle()
        return None

    @line_magic
    def ct(self, line):
        """
        Show data from detectors.

        Args:
            line (str, optional): A space-separated list of detector labels.

        """
        self.get_devices(line, 'detector')

    @line_magic
    def wa(self, line):
        """
        Show data from motors.

        Args:
            line (str, optional): A space-separated list of detector labels.
        """
        self.get_devices(line, 'motor')

    def get_devices(self, line, device_type:str):
        """
        List devices info based on type

        Args:
            line (str): A space-separated list of devices labels.
            device_type (str): 'motor' or 'detector'

        Raises:
            ValueError: If the input line contains list brackets '[]'.
        """
        # Get the current date and time
        current_datetime = datetime.now()
        # Format the date and time in European format
        formatted_datetime = current_datetime.strftime("%A, %d/%m/%Y %H:%M:%S")
        # Print the formatted dyate and time
        print(f"Date and Time: {formatted_datetime}\n")
        if line.strip():
            # Check if the line contains brackets which is not allowed
            if '[' in line or ']' in line:
                raise ValueError("It looks like you entered a list like "
                                    "`%wa [motors, detectors]` "
                                    "Magics work a bit differently than "
                                    "normal Python. Enter "
                                    "*space-separated* labels like "
                                    "`%wa motors detectors`.")
            # User has provided a white list of labels like '%wa label1 label2'
            devices = line.strip().split()
        else:
            # Show all labels if no specific labels are provided
            devices = self.label_axes_dict.keys()
        
        # Loop through each label to display positioner info
        for device in devices:
            # Retrieve the device dictionary for the current label
            device_dict = self.label_axes_dict[device]
            if device_dict['device_type'] == device_type:
                # Initialize the lines list to store formatted strings
                lines = []
                # Prepare headers and generate the line format string
                headers = ['Positioner']
                headers.extend(device_dict['headers'])
                LINE_FMT = generate_line_fmt(headers)                
                # Append the headers to the lines list
                lines.append(LINE_FMT.format(*headers))
                # Loop through each axis key in the device dictionary
                for axis_key in device_dict['axes'].keys():
                    # Prepare the line for the current axis
                    line = []
                    line.append(str(axis_key))
                    # Loop through each axis name to get its value
                    for axis_name in device_dict['axes'][axis_key]:
                        # Check if axis_name is empty and adjust full name accordingly
                        if axis_name != '':
                            axis_full_name = f'{axis_key}.{axis_name}'
                        else:
                            axis_full_name = f'{axis_key}'
                        # Get the axis value from the string method
                        axis_value = self.get_axis_from_string(axis_full_name)
                        # Round the axis value to 6 decimal places
                        if np.issubdtype(type(axis_value), np.number):
                            axis_value = np.round(axis_value, 6)
                        # Append the axis value to the line list
                        line.append(str(axis_value))
                    # Append the formatted line to the lines list
                    lines.append(LINE_FMT.format(*line))
                # Print the label and its associated positioner info
                print()
                print(f"Device : {device}")
                print('\n'.join(lines))

    def get_axis_from_string(self,axis):
        """
        Get the axis value from an axis name.

        Args:
            axis (str): The name of the axis.

        Returns:
            Any: The value of the axis.

        Raises:
            None
        """
        try:
            axis_value   = eval(axis + '.get()', self.shell.user_ns)
        except Exception as e:
            axis_value = 'Disconnected'
        return axis_value
    

def generate_line_fmt(headers):
    """
    Generates a line format string based on the given list of headers.
    
    Each header will have a corresponding format specifier with a width
    that is one character longer than the length of the header. This ensures
    that there is at least one space between each field when printed.
    
    Args:
        headers (list of str): List of header names.
    
    Returns:
        str: A format string for printing lines with the specified headers.
    
    Examples:
        >>> generate_line_fmt(['test1', 'test22'])
        '{:<6} {:<7}'
        
        >>> generate_line_fmt(['test1', 'test22', 'test236'])
        '{:<6} {:<7} {:<8}'
    """
    # Initialize an empty list to store the format parts
    line_fmt_parts = []
    
    # Loop over each header in the headers list
    for header in headers:
        # Create a format specifier for the current header
        # The width is set to the length of the header + 1
        line_fmt_parts.append(f'{{:>{len(header) + 8}}}')
    
    # Join all the format specifiers with a space in between
    line_fmt = ' '.join(line_fmt_parts)
    
    # Return the final format string
    return line_fmt



            
            

    
