from bluesky.preprocessors import (
    SupplementalData,
    baseline_wrapper,
)

from bluesky.preprocessors import (
    plan_mutator, msg_mutator,
    single_gen,
    ensure_generator,
)

from bluesky.utils import Msg
import bluesky.plan_stubs as bps


#bluesky imports
from bluesky.callbacks.best_effort import BestEffortCallback
from bluesky import RunEngine
from ophyd import Kind, Signal, Device
from pprint import pprint
#define the plan wrapper
from bluesky.preprocessors import (
    
    single_gen,
    ensure_generator,
    inject_md_wrapper,
    stage_wrapper
    
)
import inspect
from bluesky.utils import Msg

from collections import OrderedDict, deque, ChainMap
from collections.abc import Iterable
import uuid
from bluesky.utils import (normalize_subs_input, root_ancestor,
                    separate_devices,
                    Msg, ensure_generator, single_gen,
                    short_uid as _short_uid, make_decorator,
                    RunEngineControlException, merge_axis)
from functools import wraps
from bluesky.plan_stubs import (open_run, close_run, mv, pause, trigger_and_read)



def plan_mutator(plan, msg_proc):
    """
    Alter the contents of a plan on the fly by changing or inserting messages.

    Parameters
    ----------
    plan : generator

        a generator that yields messages (`Msg` objects)
    msg_proc : callable
        This function takes in a message (or additionally the previous message) 
        and specifies messages(s) to replace it with.
        
        The function must account for what type of response the
        message would prompt. For example, an 'open_run' message causes the
        RunEngine to send a uid string back to the plan, while a 'set' message
        causes the RunEngine to send a status object back to the plan. The
        function should return a pair of generators ``(head, tail)`` that yield
        messages. The last message out of the ``head`` generator is the one
        whose response will be sent back to the host plan. Therefore, that
        message should prompt a response compatible with the message that it is
        replacing. Any responses to all other messages will be swallowed. As
        shorthand, either ``head`` or ``tail`` can be replaced by ``None``.
        This means:

        * ``(None, None)`` No-op. Let the original message pass through.
        * ``(head, None)`` Mutate and/or insert messages before the original
          message.
        * ``(head, tail)`` As above, and additionally insert messages after.
        * ``(None, tail)`` Let the original message pass through and then
          insert messages after.
          
        If `msg_proc` accepts two arguments, it is provided with the current message and the previous message. 
        This setup allows the function to consider immediate historical context when deciding how to modify or replace the current message.
          
        If `msg_proc` accepts three arguments, it is provided with the current message, the previous message, 
        and the entire message stack seen so far. This allows `msg_proc` to make decisions based on a broader 
        context of the plan execution history.

        The reason for returning a pair of generators instead of just one is to
        provide a way to specify which message's response should be sent out to
        the host plan. Again, it's the last message yielded by the first
        generator (``head``).
        

    Yields
    ------
    msg : Msg
        messages from `plan`, altered by `msg_proc`

    See Also
    --------
    :func:`bluesky.plans.msg_mutator`
    """
    # internal stacks
    msgs_seen = dict()
    msgs_seen_list = []
    plan_stack = deque()
    result_stack = deque()
    tail_cache = dict()
    tail_result_cache = dict()
    exception = None

    parent_plan = plan
    ret_value = None
    # seed initial conditions
    plan_stack.append(plan)
    result_stack.append(None)

    while True:
        # get last result
        if exception is not None:
            # if we have a stashed exception, pass it along
            try:
                msg = plan_stack[-1].throw(exception)
            except StopIteration as e:
                # discard the exhausted generator
                exhausted_gen = plan_stack.pop()
                # if this is the parent plan, capture it's return value
                if exhausted_gen is parent_plan:
                    ret_value = e.value

                # if we just came out of a 'tail' generator,
                # discard its return value and replace it with the
                # cached one (from the last message in its paired
                # 'new_gen')
                if id(exhausted_gen) in tail_result_cache:
                    ret = tail_result_cache.pop(id(exhausted_gen))

                result_stack.append(ret)
                

                if id(exhausted_gen) in tail_cache:
                    gen = tail_cache.pop(id(exhausted_gen))
                    if gen is not None:
                        plan_stack.append(gen)
                        saved_result = result_stack.pop()
                        tail_result_cache[id(gen)] = saved_result
                        # must use None to prime generator
                        result_stack.append(None)

                if plan_stack:
                    
                    continue
                else:
                    return ret_value
            except Exception as e:
                # if we catch an exception,
                # the current top plan is dead so pop it
                plan_stack.pop()
                if plan_stack:
                    # stash the exception and go to the top
                    exception = e
                    continue
                else:
                    raise
            else:
                exception = None
        else:
            ret = result_stack.pop()
            try:
                
                msg = plan_stack[-1].send(ret)
               
            except StopIteration as e:
                # discard the exhausted generator
                exhausted_gen = plan_stack.pop()
                # if this is the parent plan, capture it's return value
                if exhausted_gen is parent_plan:
                    ret_value = e.value

                # if we just came out of a 'tail' generator,
                # discard its return value and replace it with the
                # cached one (from the last message in its paired
                # 'new_gen')
                if id(exhausted_gen) in tail_result_cache:
                    ret = tail_result_cache.pop(id(exhausted_gen))

                result_stack.append(ret)

                if id(exhausted_gen) in tail_cache:
                    gen = tail_cache.pop(id(exhausted_gen))
                    if gen is not None:
                        plan_stack.append(gen)
                        saved_result = result_stack.pop()
                        tail_result_cache[id(gen)] = saved_result
                        # must use None to prime generator
                        result_stack.append(None)

                if plan_stack:
                    continue
                else:
                    return ret_value
            except Exception as ex:
                # we are here because an exception came out of the send
                # this may be due to
                # a) the plan really raising or
                # b) an exception that came out of the run engine via ophyd

                # in either case the current plan is dead so pop it
                failed_gen = plan_stack.pop()
                if id(failed_gen) in tail_cache:
                    gen = tail_cache.pop(id(failed_gen))
                    if gen is not None:
                        plan_stack.append(gen)
                # if there is at least
                if plan_stack:
                    exception = ex
                    continue
                else:
                    raise ex
        # if inserting / mutating, put new generator on the stack
        # and replace the current msg with the first element from the
        # new generator
        if id(msg) not in msgs_seen:
            
            msgs_seen[id(msg)] = msg
            
            # Use the id as a hash, and hold a reference to the msg so that
            # it cannot be garbage collected until the plan is complete.
            
            # Additionally keep track of all previous messages
            msgs_seen_list.append(msg)
            args_list = inspect.getfullargspec(msg_proc)
            args_num = len(args_list[0])
   
            #if msg_proc has two arguements, give it the previous message as the second
            if args_num == 1:
                new_gen, tail_gen = msg_proc(msg)
            elif args_num == 2:
                if len(msgs_seen) >2 :
                    new_gen, tail_gen = msg_proc(msg,msgs_seen_list[-2])
                else:
                    new_gen, tail_gen = msg_proc(msg,msgs_seen_list[-1])
            elif args_num == 3:
                if len(msgs_seen) >2:
                    new_gen, tail_gen = msg_proc(msg,msgs_seen_list[-2],msgs_seen_list)
                else:
                    new_gen, tail_gen = msg_proc(msg,msgs_seen_list[-1],msgs_seen_list)            
            # mild correctness check
            if tail_gen is not None and new_gen is None:
                new_gen = single_gen(msg)
            if new_gen is not None:
                # stash the new generator
                plan_stack.append(new_gen)
                # put in a result value to prime it
                result_stack.append(None)
                # stash the tail generator
                tail_cache[id(new_gen)] = tail_gen
                # go to the top of the loop
                continue
        try:
            # yield out the 'current message' and collect the return
            inner_ret = yield msg
        except GeneratorExit:
            # special case GeneratorExit.  We must clean up all of our plans
            # and exit with out yielding anything else.
            for p in plan_stack:
                p.close()
            raise
        except Exception as ex:
            if plan_stack:
                exception = ex
                continue
            else:
                raise
        else:

            result_stack.append(inner_ret)

                
def check_if_continuous_scan(msg_stack):
    """Check if the message stack indicates that this is a continuous scan.

    This function iterates through each message in the `msg_stack` list looking for a message with the `command` attribute set to "open_run". It then checks if this message's `kwargs` attribute contains the key `['plan_name']` with a value of either 'mov_count' or 'flying'. If such a message is found, it returns True, indicating the scan is continuous. Otherwise, it returns False.

    Args:
        msg_stack (list): A list of message objects to be checked.

    Returns:
        bool: True if the scan is continuous ('mov_count' or 'flying' is found), False otherwise.

    """
    # Iterate through each message in the msg_stack list
    for m in msg_stack:
        # Check if the message has an attribute 'command'
        if hasattr(m, 'command') and m.command == "open_run":
            # Check if the message has 'kwargs' and the 'plan_name' key with the specified values
            if hasattr(m, "kwargs") and m.kwargs.get('plan_name') in ["mov_count", "flying"]:
                return True
    # If it is not a continuous scan, return False
    return False
            
def repeat_if_inj(plan, last_inj, protection_window):
    """
    Modifies a given plan to include a check that potentially repeats part of the plan based on the time since the last injection and a defined protection window. 

    This function wraps the original plan with a mutator that listens for specific conditions in the message stack. If the conditions are met (not a continuous scan, specific sequence of commands observed, and the time since the last injection is less than the protection window), it repeats a segment of the plan after waiting until the protection window has passed. This is intended to protect against too frequent injections by ensuring there's a minimum time interval between them.

    Args:
        plan (generator): The original plan to be executed, which yields messages.
        last_inj (object): An object that provides the time since the last injection, expected to have a `.get()` method that returns time in tenths of seconds.
        protection_window (int): The minimum allowed time (in seconds) between injections to enforce protection.

    Returns:
        generator: A new plan that includes the modified behavior to check for injection timing and potentially repeat part of the plan.

    Inner Function:
        repeat_trigger(msg, last_msg, msg_stack): A closure used to mutate the original plan. It checks the message stack and the time since the last injection against the protection window. If conditions are met, part of the plan is repeated; otherwise, the original plan continues.

    Note:
        - This function assumes the presence of a `plan_mutator` function that applies the `repeat_trigger` function to the original plan.
        - The inner workings, such as the specific conditions checked and the mechanism of repeating plan segments, depend on external functions not defined here.
    """   
    def repeat_trigger(msg, last_msg, msg_stack):
        continuos_scan = check_if_continuous_scan(msg_stack)       
        if len(msg_stack)>2 and not continuos_scan:
            if msg_stack[-1].command == 'create' and msg_stack[-2].command == 'wait':
                trigger_msg = find_trigger_and_wait_msg(msg_stack)
                # last_inj is in tenth of seconds, 
                # so divide it by 10 to get it in seconds
                seconds_since_last_inj = last_inj.get()/10
                # print(f'seconds_since_last_inj {seconds_since_last_inj}')
                if seconds_since_last_inj< protection_window:
                    print(f"Last Injection happened {int(seconds_since_last_inj)} seconds ago, wait time is {protection_window}")
                    print(f"The plan will resume in {int(protection_window - seconds_since_last_inj)} seconds.")
                    while last_inj.get()/10< protection_window:
                        pass
                    def new_gen():
                        yield from ensure_generator(trigger_msg)
                        yield msg
                    return new_gen(), None
                else:
                    return None, None
            else:
                    return None, None
        else:
            return None, None
        # else:
        #         return None, None

    plan1 = plan_mutator(plan, repeat_trigger)


    
    return (yield from plan1)

class SupplemetalDataInjection(SupplementalData):
    """Supplemental data for for repeating measurements if an injection happened.


    Args:
            last_inj (Signal): The Ophyd signal connnected to the lastShot count
            protection_window (int or float): the time to wait after the injection to 
                                        repeat the measurement 

    Usage:

            from beamlinetools.preprocessor.avoid_injection import SupplemetalDataInjection
            repeat_trigger_injection = SupplemetalDataInjection(last_inj=last_inj, protection_window=2)
            RE.preprocessors.append(repeat_trigger_injection)

    """    

    def __init__(self, *args, last_inj,protection_window, **kwargs):     
        super().__init__(*args, **kwargs)
        self.last_inj = last_inj
        self.protection_window = protection_window
        
    def __call__(self, plan):
        plan = repeat_if_inj(plan, self.last_inj,self.protection_window)
        return (yield from plan)
    
    def change_protection_window(self, protection_window):
        self.protection_window = protection_window


def find_trigger_and_wait_msg(msg_stack):
    """
    Iterate through the message stack to identify the trigger and 
    wait messages, returning them.

    This is an example of the sequence of messages in the stack 
    for a scan with one detector and one motor 
    (therefore groups of two trigger are expected in this case):
    ...
    checkpoint
    set
    wait
    trigger -> will be returned
    trigger -> will be returned
    wait    -> will be returned
    create ---> the function should be executed before this message
    read
    read
    save
    checkpoint
    ...

    Args:
        msg_stack (list): A list of message objects, 
        where each message object should have a 'command' attribute.

    Returns:
        list: A list of message objects that includes the trigger 
        messages followed by one wait message, in the order they 
        appear in the original stack.
    """   
    wait_and_trigger_msg_list = []
    first_wait_found = False
    # Iterate through each message in the stack
    for m in reversed(msg_stack):
        # here we find the first wait message
        # and add it to the list of messages
        if m.command == 'wait' and not first_wait_found:
            wait_and_trigger_msg_list.append(m)
            first_wait_found = True
        # once we found the first wait message,
        # we add all the trigger messages to the list
        elif m.command == 'trigger' and first_wait_found:
            wait_and_trigger_msg_list.append(m)
            first_wait_found = True
        # as soon as we find a message that is not trigger
        # we are done
        elif m.command != 'trigger' and first_wait_found:
            break
    # we return the list in inverse order, because we want to have
    # first the trigger messages and then the wait
    # print("messages to be repeated")
    # for m in wait_and_trigger_msg_list:
    #     print(m.command)
    return reversed(wait_and_trigger_msg_list)