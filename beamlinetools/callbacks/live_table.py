import os
import time as ttime
import warnings
from collections import OrderedDict, namedtuple
from datetime import datetime
from decimal import Decimal

from bluesky.callbacks.core import make_class_safe, logger, CallbackBase, ensure_uid

def get_obj_fields(fields):
    """
    If fields includes any objects, get their field names using obj.describe()

    ['det1', det_obj] -> ['det1, 'det_obj_field1, 'det_obj_field2']"
    """
    string_fields = []
    for field in fields:
        if isinstance(field, str):
            string_fields.append(field)
        else:
            try:
                field_list = field.describe().keys()
            except AttributeError:
                raise ValueError(  # noqa: B904
                    "Fields must be strings or objects with a 'describe' method that return a dict."
                )
            string_fields.extend(field_list)
    return string_fields

def format_e(n, prec=4):
    a = f"{n:.{prec}E}"
    return a.split('E')[0].rstrip('0').rstrip('.') + 'E' + a.split('E')[1]



@make_class_safe(logger=logger)
class LiveTable(CallbackBase):
    """Live updating table

    Parameters
    ----------
    fields : list
         List of fields to add to the table.

    stream_name : str, optional
         The event stream to watch for

    print_header_interval : int, optional
         Reprint the header every this many lines, defaults to 50

    min_width : int, optional
         The minimum width is spaces of the data columns.  Defaults to 12

    default_prec : int, optional
         Precision to use if it can not be found in descriptor, defaults to 3

    extra_pad : int, optional
         Number of extra spaces to put around the printed data, defaults to 1

    separator_lines : bool, optional
        Add empty lines before and after the printed table, default True

    logbook : callable, optional
        Must take a sting as the first positional argument

           def logbook(input_str):
                pass

    out : callable, optional
        Function to call to 'print' a line.  Defaults to `print`
    """
    
    _LOW_BOUND = 1e-4
    
    _UP_BOUND = 1e4

    _FMTLOOKUP = {
        "s": "{pad}{{{k}: >{width}.{prec}{dtype}}}{pad}",
        "f": "{pad}{{{k}: >{width}.{prec}{dtype}}}{pad}",
        "g": "{pad}{{{k}: >{width}.{prec}{dtype}}}{pad}",
        "d": "{pad}{{{k}: >{width}{dtype}}}{pad}",
    }
    _FMT_MAP = {
        "number": "f",
        "integer": "d",
        "string": "s",
    }
    _fm_sty = namedtuple("fm_sty", ["width", "prec", "dtype"])  # type: ignore
    water_mark = "{st[plan_type]} {st[plan_name]} ['{st[uid]:.8s}'] (scan num: {st[scan_id]})"
    ev_time_key = "SUPERLONG_EV_TIMEKEY_THAT_I_REALLY_HOPE_NEVER_CLASHES"

    def __init__(
        self,
        fields,
        *,
        stream_name="primary",
        print_header_interval=50,
        min_width=12,
        default_prec=3,
        extra_pad=1,
        separator_lines=True,
        logbook=None,
        out=print,
    ):
        super().__init__()
        self._header_interval = print_header_interval
        # expand objects
        self._fields = get_obj_fields(fields)
        self._stream = stream_name
        self._start = None
        self._stop = None
        self._descriptors = set()
        self._pad_len = extra_pad
        self._extra_pad = " " * extra_pad
        self._min_width = min_width
        self._default_prec = default_prec
        self._separator_lines = separator_lines
        self._format_info = OrderedDict(
            [
                ("seq_num", self._fm_sty(10 + self._pad_len, "", "d")),
                (self.ev_time_key, self._fm_sty(10 + 2 * extra_pad, 10, "s")),
            ]
        )
        self._rows = []
        self.logbook = logbook
        self._sep_format = None
        self._out = out

    def descriptor(self, doc):
        def patch_up_precision(p):
            try:
                return int(p)
            except (TypeError, ValueError):
                return self._default_prec

        if doc["name"] != self._stream:
            return

        self._descriptors.add(doc["uid"])
        
        dk = doc["data_keys"]
        for k in self._fields:
            width = max(self._min_width, len(k) + 2, self._default_prec + 1 + 2 * self._pad_len)
            try:
                dk_entry = dk[k]
            except KeyError:
                # this descriptor does not know about this key
                continue

            if dk_entry["dtype"] not in self._FMT_MAP:
                warnings.warn(  # noqa: B028
                    "The key {} will be skipped because LiveTable "
                    "does not know how to display the dtype {}"
                    "".format(k, dk_entry["dtype"])
                )
                continue

            prec = patch_up_precision(dk_entry.get("precision", self._default_prec))
            fmt = self._fm_sty(width=width, prec=prec, dtype=self._FMT_MAP[dk_entry["dtype"]])

            self._format_info[k] = fmt

        self._sep_format = "+" + "+".join("-" * f.width for f in self._format_info.values()) + "+"
        self._main_fmnt = "|".join(
            "{{: >{w}}}{pad}".format(w=f.width - self._pad_len, pad=" " * self._pad_len)
            for f in self._format_info.values()
        )
        headings = [k if k != self.ev_time_key else "time" for k in self._format_info]
        self._header = "|" + self._main_fmnt.format(*headings) + "|"
        self._data_formats = OrderedDict(
            (
                k,
                self._FMTLOOKUP[f.dtype].format(
                    k=f"h{str(hash(k))}",
                    width=f.width - 2 * self._pad_len,
                    prec=f.prec,
                    dtype=f.dtype,
                    pad=self._extra_pad,
                ),
            )
            for k, f in self._format_info.items()
        )

        self._count = 0

        if self._separator_lines:
            self._print("\n")
        self._print(self._sep_format)
        self._print(self._header)
        self._print(self._sep_format)
        super().descriptor(doc)

    def event(self, doc):
        try:
            if ensure_uid(doc["descriptor"]) not in self._descriptors:
                return
            # shallow copy so we can mutate
            data = dict(doc["data"])

            self._count += 1
            if not self._count % self._header_interval:
                self._print(self._sep_format)
                self._print(self._header)
                self._print(self._sep_format)
            fmt_time = str(datetime.fromtimestamp(doc["time"]).time())
            data[self.ev_time_key] = fmt_time
            data["seq_num"] = doc["seq_num"]
            cols = []
            
            # Use a list comprehension to build the 'cols' list by iterating over items in '_data_formats'.
            cols = [
                # For each key ('k') and formatting function ('f') in '_data_formats' dictionary:
                (
                    # Check if the data needs formatting as a string or is within bounds.
                    # If 'k' represents a string data type or the value is within specified bounds:
                    f.format(**{f"h{str(hash(k))}": data[k]})
                    if isinstance(data[k], str) or self._LOW_BOUND < data[k] < self._UP_BOUND
                    # If not a string and out of bounds, format using 'format_e' and right-justify to match desired width.
                    else format_e(data[k], 4).rjust(self._format_info[k].width)
                )
                # Check if 'k' exists in 'data' and if it's marked as filled in 'doc'.
                # 'doc.get("filled", {})' retrieves a dictionary for filled status or an empty one if not present.
                # 'doc.get("filled", {}).get(k, True)' returns True if 'k' should be filled; defaults to True if not specified.
                if k in data and doc.get("filled", {}).get(k, True)
                # If 'k' is not in 'data' or not to be filled as specified in 'doc', create a placeholder string of spaces.
                else " " * self._format_info[k].width
                for k, f in self._data_formats.items()  # Loop over each item in '_data_formats'.
            ]

            self._print("|" + "|".join(cols) + "|")
        except Exception as ex:
            if self.log is not None:
                self.log.exception(ex)
            print(ex)
            self._print(f"{{k:*^{self._min_width}}}".format(k=" failed to format row "))
            if os.environ.get("BLUESKY_DEBUG_CALLBACKS", False):
                raise ex
        super().event(doc)

    def stop(self, doc):
        if ensure_uid(doc["run_start"]) != self._start["uid"]:
            return

        # This sleep is just cosmetic. It improves the odds that the bottom
        # border is not printed until all the rows from events are printed,
        # avoiding this ugly scenario:
        #
        # |         4 | 22:08:56.7 |      0.000 |
        # +-----------+------------+------------+
        # generator scan ['6d3f71'] (scan num: 1)
        # Out[2]: |         5 | 22:08:56.8 |      0.000 |
        ttime.sleep(0.1)

        if self._sep_format is not None:
            self._print(self._sep_format)
        self._stop = doc

        wm = self.water_mark.format(st=self._start)
        self._out(wm)
        if self.logbook:
            self.logbook("\n".join([wm] + self._rows))
        if self._separator_lines:
            self._print("\n")
        super().stop(doc)

    def start(self, doc):
        self._rows = []
        self._start = doc
        self._stop = None
        self._sep_format = None
        super().start(doc)

    def _print(self, out_str):
        self._rows.append(out_str)
        self._out(out_str)
