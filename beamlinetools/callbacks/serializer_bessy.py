from suitcase.specfile import *

class SerializerBessy(Serializer):

    def event(self, doc):
        if (self._baseline_descriptor and
                doc['descriptor'] == self._baseline_descriptor['uid']):
            self._num_baseline_events_received += 1
            self._baseline_event = doc
            return
        # Write the scan header as soon as we get the first event.  If it is
        # not the baseline event, then sorry! You need to give me that before
        # any primary events.
        if self._has_not_written_file_header:
            # maybe write a new file header if there is not one already
            self._write_new_header()
            self._has_not_written_file_header = False
        if self._has_not_written_scan_header:
            # write the scan header with whatever information we currently have
            scan_header = to_spec_scan_header(self._start,
                                              self._primary_descriptor,
                                              self._baseline_event)
            self._file.write(scan_header)
            self._has_not_written_scan_header = False

        if doc['descriptor'] != self._primary_descriptor['uid']:
            err_msg = (
                "The DocumentToSpec callback is not designed to handle more "
                "than one event stream.  If you need this functionality, please "
                "request it at https://github.com/NSLS-II/suitcase/issues. "
                "Until that time, this DocumentToSpec callback will raise a "
                "NotImplementedError if you try to use it with two event "
                "streams.")
            raise NotImplementedError(err_msg)
        self._num_events_received += 1
        # now write the scan data line
        scan_data_line = to_spec_scan_data(self._start,
                                           self._primary_descriptor, doc)
        # new bessy version, remove \n
        # self._file.write(scan_data_line + '\n')
        self._file.write(scan_data_line)
        if self._flush:
            self._file.flush()