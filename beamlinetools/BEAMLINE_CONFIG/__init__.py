from .base import *
from .beamline import *
from .plans import *
from .tools import *
from .baseline import *
from .file_export import *
from .magics import *
from .suspenders import *
from .hooks import *
#from .authentication_and_metadata import *

# Execute code in the scope of the IPyhton namespace just created

# Delete plan functions reference in the current scope for automagic to work
imported_objects = get_imported_objects('/opt/bluesky/beamlinetools/beamlinetools/BEAMLINE_CONFIG/plans.py')
plan_names = [obj for obj in imported_objects if not obj.startswith('_')]
for name in plan_names:
    exec(f'del {name}')