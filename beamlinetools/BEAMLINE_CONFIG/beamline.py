"""Module:      beamline.py

Description:    Mandatory and optional imports.
                Instantiation of beamline devices and waiting for the devices to connect. Instatiation is based on the device configuration y(a)ml file and rml file.
                Inclusion of instantiated devices in the global scope to enable direct access via instance names only, e.g. v3.
                    (Access to instantiated devices is also possible via a "devices_dictionary" dictionary, e.g. devices_dictionary["v3"])
                Some additional assignments for some of the instantiated devices.
"""

# Mandatory import of auxiliary function: "add_to_global_scope"
from bessyii.utils.helper import add_to_global_scope

# Mandatory import of functions for beamline device instantiation
from bessyii_devices_instantiation.beamline_devices import (
    instantiate,
    wait_for_device_connection,
)

# Optional import of simulated ophyd devices for test purposes
from ophyd.sim import det, det1, det2, det3, noisy_det
from ophyd.sim import motor, motor1, motor2, motor3

print("\n\nLOADING beamline.py")

# Instantiate beamline devices
devices_dictionary: dict[object] = instantiate()

# Wait for instantiated devices to be connected
wait_for_device_connection(devices_dictionary)

# Add devices to the global scope
add_to_global_scope(devices_dictionary, globals())

# Additional assignments for hios
if "hios" in devices_dictionary.keys():
    hios.hm2 = hios.hm2pseudo.hm2
    hios.hm2_absolute = hios.hm2pseudo.rhm2
