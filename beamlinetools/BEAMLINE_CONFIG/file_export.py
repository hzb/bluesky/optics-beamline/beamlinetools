from .base import RE
from beamlinetools.data_management.data_structure import OpticsDataStructure

print('\n\nLOADING file_export.py')

mds = OpticsDataStructure(RE)
change_user = mds.change_user 
change_experiment = mds.change_experiment
