# in this file all the imports useful to the beamline

# imports from beamlinetools


#imports from bessyii
#from .base import *
#from bessyii.plans.n2fit import N2_fit
#N2fit_class = N2_fit(db)
#fit_n2 = N2fit_class.fit_n2


from beamlinetools.utils.selector import MotorSelector
from .base import RE
from .beamline import *
# from beamlinetools.utils.pbar_bessy import ProgressBarManager 
# RE.waiting_hook = ProgressBarManager()

print('\n\nLOADING tools.py')

try:
    c = MotorSelector(RE, "Detector", ['x [mm]', 'y [deg]'], 
                    [r.det1, r.twt], 
                    '/opt/bluesky/beamlinetools/beamlinetools/utils/detector_values.csv', 
                    reset_axis=True, 
                    additional_print_statement='New Io abs. coordinates')
    Detector = c.selector
    print('Detector function activated')
except Exception as e:
    print(f'Detector function not active: {e}')

try:
    s1 = MotorSelector(RE, "Slit1", ['Position [mm]'], [fsu.slt1], '/opt/bluesky/beamlinetools/beamlinetools/utils/slit1.csv')
    Slit1 = s1.selector
    print('Slit1 function activated')
except Exception as e:
    print(f'Slit1 function not active: {e}')

try:
    s2 = MotorSelector(RE, "Slit2", ['Position [mm]'], [fsu.slt2], '/opt/bluesky/beamlinetools/beamlinetools/utils/slit2.csv')
    Slit2 = s2.selector
    print('Slit2 function activated')
except Exception as e:
    print(f'Slit2 function not active: {e}')

try:
    f1 = MotorSelector(RE, "Filter1", ['Position [mm]'], [fsu.flt1], '/opt/bluesky/beamlinetools/beamlinetools/utils/flt1.csv')
    Filter1 = f1.selector
    print('Filter1 function activated')
except Exception as e:
    print(f'Filter1 function not active: {e}')

try:
    f2 = MotorSelector(RE, "Filter2", ['Position [mm]'], [fsu.flt2], '/opt/bluesky/beamlinetools/beamlinetools/utils/flt2.csv')
    Filter2 = f2.selector
    print('Filter2 function activated')
except Exception as e:
    print(f'Filter2 function not active: {e}')

from beamlinetools.utils.script_load_helper import simple_load
SL = simple_load(user_script_location='/opt/bluesky/user_scripts/')
load_user_script = SL.load_script


from .beamline import *
def settle_time(seconds):
    for device in devices_dictionary.values():
        try:
            if device.name=='pgm':
                device.en.settle_time=seconds
                print(f'{device.en.name} settle time = {seconds} seconds')
            else:
                device.settle_time=seconds
                print(f'{device.name} settle time = {seconds} seconds')
        except Exception as e:
            print(f'Could not set settle time on {device.name}: {e}')


    
settle_time(0.5)


from .beamline import next_injection
from beamlinetools.preprocessor.avoid_injection import SupplemetalDataInjection
from .base import RE


if 'next_injection' in devices_dictionary.keys():
    repeat_trigger_injection = SupplemetalDataInjection(last_inj=next_injection.time_last_injection, protection_window=2)

    def wait_for_injection(delay=None):
        if repeat_trigger_injection in RE.preprocessors and (delay == None or delay==0):
            RE.preprocessors.remove(repeat_trigger_injection)
            print (f'Wait for injection is deactivated') 
        else:
            if delay == None:
                delay = 2
            if type(delay) == int or type(delay) == float:  
                repeat_trigger_injection.protection_window=delay    
                RE.preprocessors.append(repeat_trigger_injection)
                print (f'Wait for injection is active with a delay of {delay} seconds') 
            else:
                raise ValueError(f'The delay can be only a float or an int. You passed {delay} of type {type(delay)}')

    print('wait_for_injection is available')

else:
    print('wait_for_injection is NOT available')