from bluesky import RunEngine
from os.path import expanduser
from tiled.client import from_uri

print('\n\nLOADING base.py')
# Start the tiled client 
client = from_uri("http://localhost:8000", api_key="secret")

RE = RunEngine({})

# from bluesky.callbacks.best_effort import BestEffortCallback
from beamlinetools.callbacks.best_effort import BestEffortCallback
bec = BestEffortCallback()

# Send all metadata/data captured to the BestEffortCallback.
RE.subscribe(bec)

bec.overplot=True
# Database definition, change catalog_name for the actual
# name of the database
#import databroker
#db = databroker.catalog["catalog_name"]
#RE.subscribe(db.v1.insert)

# Temporary database, once mongo is installed and a database created use
 # comment the following lines 
#from databroker import Broker
#db = Broker.named('temp')
#RE.subscribe(db.insert)

def post_document(name,doc):
    client.post_document(name,doc)

RE.subscribe(post_document)


# If you need debug messages from the RE then uncomment this
#from bluesky.utils import ts_msg_hook
#RE.msg_hook = ts_msg_hook



# Configure persistence between sessions of metadata
# change beamline_name to the name of the beamline
from bluesky.utils import PersistentDict
import os
cwd = os.getcwd()
RE.md = PersistentDict(expanduser('/opt/bluesky/data/persistence/beamline/'))


# import databroker.core
# handler_registry = databroker.core.discover_handlers()

# import matplotlib
import matplotlib

