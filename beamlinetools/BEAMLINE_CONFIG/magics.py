from beamlinetools.magics.standard_magics_utils import get_imported_objects, create_magics_from_list, PlotSelect
# from beamlinetools.magics.peakinfo import PeakInfoMagic
from beamlinetools.magics.standard_magics import BlueskyMagicsBessy
from IPython import get_ipython
from .base import RE
from .devices_configuration import devices_dict




print('\n\nLOADING magics.py')

from .plans import *
count_plan  = count 
scan_plan   = scan
ascan_plan  = ascan
dscan_plan  = dscan
dmesh_plan  = dmesh
amesh_plan  = amesh
a2scan_plan = a2scan
d2scan_plan = d2scan
tweak_plan  = tweak

imported_objects = get_imported_objects('/opt/bluesky/beamlinetools/beamlinetools/BEAMLINE_CONFIG/plans.py')
plan_names = [obj for obj in imported_objects if not obj.startswith('_')]
create_magics_from_list(plan_names)

from .beamline import *

plt_det = []
for device in devices_dictionary.values():  
    try:
        if device.md["silent"] == 'True':
            if device.name == 'accelerator':
                plt_det.append(device.current)
            else:
                plt_det.append(device)
            print(f"Device: {device.name} added to the list 'plot_detectors'.")
        else:
            pass
    except KeyError as e:
        print(f"Device: {device.name} NOT added to the list 'plot_detectors' {e}.")

if len(plt_det)>0:        
    ps = PlotSelect(RE,get_ipython().user_ns, plt_det)
    plotselect = ps.plotselect
        

get_ipython().register_magics(BlueskyMagicsBessy(RE, 
                                get_ipython(), 
                                database_name ="client", 
                                label_axes_dict=devices_dict))

# This magic does not work, we use bec.peakinfo at the moment
# get_ipython().register_magics(PeakInfoMagic)
# usage: peakinfo

